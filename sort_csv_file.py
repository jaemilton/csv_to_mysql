from math import fabs
from common_lib.common_base import get_input_parameter_value, valid_mandatory_parameters, convert_string_to_array
from common_lib.common_error import BadUserInputError
from common_lib.csv_sort import CsvSort
import sys
# import csv
# from csvsort import csvsort

def start(argv):
    if (('-h' in argv) or ('-?' in argv)):
        print("""
        python3 sort_csv_file.py -s SOURCE_CSV_PATH -d DESTINATION_CSV_PATH -i LIST_OF_INDEXS_TO_SORT [-cd CSV_COLUMN_DELIMITER] [-l CSV_LINE_SEPARATOR] [-h|-?]
        Program to load a csv to a mysql table
        Parameters:
            -s SOURCE_CSV_PATH  --> mandatory, CSV file path to be sorted
            -d DESTINATION_CSV_PATH --> mandatory, CSV file path that will be created with the sorted data
            -i LIST_OF_INDEXS_TO_SORT --> mandatory, list of column indexs to be used to sort data separated by semi comma (;), Ex 0;3
            -cd CSV_COLUMN_DELIMITER --> optional, if not set will be semi-comma (;)
            -l CSV_LINE_SEPARATOR --> optional, if not set will be semi-comma (\r\n)
            -h or -? help
        """)
    
    elif not valid_mandatory_parameters(argv, ['-s', '-d', '-i']):
        raise BadUserInputError(
            "Input error. To run, call as python3 sort_csv_file.py -s SOURCE_CSV_PATH -d DESTINATION_CSV_PATH -i LIST_OF_INDEXS_TO_SORT [-cd CSV_COLUMN_DELIMITER] [-l CSV_LINE_SEPARATOR] [-h|-?]")

    else:
        source_csv_path = get_input_parameter_value(argv,'-s')
        destination_csv_path = get_input_parameter_value(argv,'-d')
        key_column_indexs = convert_string_to_array(get_input_parameter_value(argv,'-i'), ';')
        csv_column_delimiter_param = get_input_parameter_value(argv,'-cd')
        csv_column_delimiter = ";" if csv_column_delimiter_param is None else csv_column_delimiter_param
        csv_line_separator_param = get_input_parameter_value(argv,'-l')
        csv_line_separator = "\r\n" if csv_line_separator_param is None else csv_line_separator_param


    # #after you assign info result to sortedlist
    # with open(source_csv_path, newline='') as csvfile:
    #     spamreader = csv.reader(csvfile, delimiter=';', quotechar='"')
    #     sortedlist = sorted(spamreader, key=lambda row: row[1], reverse=False)
    #     print()

    # csvsort(source_csv_path, [1], output_filename=destination_csv_path, has_header=False, delimiter=csv_column_delimiter)

    csv_sort = CsvSort(source_csv_path=source_csv_path, 
                        sorted_destination_csv_path=destination_csv_path,
                        csv_column_delimiter=csv_column_delimiter,
                        csv_line_separator=csv_line_separator,
                        sort_colunms_index=key_column_indexs)
    csv_sort.start()

start(sys.argv)
