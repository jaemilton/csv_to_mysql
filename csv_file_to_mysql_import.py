#!/usr/bin/python3
# -*- coding: UTF-8 -*-

from common_lib.common_base import get_input_parameter_value, valid_mandatory_parameters, print_sql_durations
from common_lib.common_error import BadUserInputError
import os, sys
from datetime import datetime
from common_lib.mysql_helper import MySqlHelper
from common_lib.log_helper import LogHelper
from os.path import dirname, abspath
from dotenv import load_dotenv

path = dirname(abspath(__file__)) + '/.env'
load_dotenv(path)

log_helper:LogHelper = None

def load_csv_to_table(csv_path: str, number_of_rows_to_ignore: int)-> None: 
    if not os.path.exists(csv_path):
        raise BadUserInputError(
            f"ERROR: The file {csv_path} doesn't exists")

    start_date_time = datetime.now()
    log_helper.print_log(f"Start date/time {start_date_time}") 
    csv_full_path = os.path.abspath(csv_path).replace('\\', '/')
    
    sql = f"""LOAD DATA CONCURRENT LOCAL INFILE '{csv_full_path}'
    INTO TABLE TB_001
    FIELDS TERMINATED BY ';'
    OPTIONALLY ENCLOSED BY '"'
    LINES TERMINATED BY '\\r\\n'
    IGNORE {number_of_rows_to_ignore} LINES;;"""


    my_sql_helper = MySqlHelper(host = os.getenv("MYSQL_HOSTNAME"),
                                    port = int(os.getenv("MYSQL_PORT")),
                                    user = os.getenv("MYSQL_USER"),
                                    passwd = os.getenv("MYSQL_PASSWD"),
                                    db = os.getenv("MYSQL_SCHEMA"),
                                    local_infile = 1)
    
    my_sql_helper.prep_and_exec_sql(sql_query=sql, sql_durations_text=f"LOAD DATA CONCURRENT LOCAL INFILE '{csv_full_path}'")

    end_date_time = datetime.now() 
    print_sql_durations(my_sql_helper, log_helper)
    log_helper.print_log(f"Finish date/time {end_date_time}") 
    log_helper.print_log(f"Total time = {end_date_time - start_date_time}")


def start(argv):
    global log_helper
    if (('-h' in argv) or ('-?' in argv)):
        log_helper.print_log("""
        python3 csv_file_to_mysql_import.py -p CSV_FULL_PATH [-i FIRST_LINES_TO_IGNORE] [-h|-?]
        Program to load a csv to a mysql table
        Parameters:
            -p CSV_FULL_PATH  --> mandatory, if not specified will generate csv on current path
            -i FIRST_LINES_TO_IGNORE --> optional, number of fist lines of csv files to be ignores on load, if no specified will be set to 0
            -h or -? help
        """)
    
    elif not valid_mandatory_parameters(argv, ['-p']):
        raise BadUserInputError(
            "Input error. To run, call as python3 csv_file_to_mysql_import.py -p CSV_FULL_PATH [-n LINES_BY_INSERT] [-h|-?]")

    else:
          csv_path = get_input_parameter_value(argv,'-p')
          number_of_rows_to_ignore_param = get_input_parameter_value(argv,'-i')
          number_of_rows_to_ignore = 0 if number_of_rows_to_ignore_param is None else int(number_of_rows_to_ignore_param)

    log_helper = LogHelper(f"{datetime.now().strftime('%Y%m%d%H%M%S')}_{os.path.basename(csv_path).replace('.csv','')}_csv_file_to_mysql_import.log")
    load_csv_to_table(csv_path, number_of_rows_to_ignore)

start(sys.argv)



