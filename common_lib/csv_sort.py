#!/usr/bin/python3
# -*- coding: UTF-8 -*-

from array import array
from common_lib.common_error import BadUserInputError
from common_lib.process_bar import ProcessBar
import os, csv
from datetime import datetime
import collections

class CsvSort(object):
  """
      Sort a csv file content by some column indexs
      @params:
          source_csv_path                   - Required  : source csv to be sorted
          sorted_destination_csv_path       - Required  : sorted destination csv
          sort_colunms_index                - Required  : sort column indexs, (e.g. [0,2]) (Array)
          csv_column_delimiter              - Optional  : column delimiter character (e.g. ",", ";") (Str)
          csv_line_separator                - Optional  : end character (e.g. "\r", "\r\n") (Str)
      """

  def __init__(self, 
                source_csv_path: str, 
                sorted_destination_csv_path: str,
                sort_colunms_index: array,
                csv_column_delimiter=";",
                csv_line_separator: str = os.linesep) -> None:

    self.source_csv_path:str = source_csv_path
    self.sorted_destination_csv_path:str = sorted_destination_csv_path
    self.sort_colunms_index:array = list(map(int, sort_colunms_index))
    self.csv_column_delimiter = csv_column_delimiter
    self.csv_line_sep:str = csv_line_separator
    self.keys:dict={}
    self.row_count:int  = 0
    self.max_sort_index:int  = max(self.sort_colunms_index)

    if not os.path.exists(self.source_csv_path):
        raise BadUserInputError(
            f"ERROR: The file {self.source_csv_path} doesn't exists")

    if os.path.exists(self.sorted_destination_csv_path):
        raise BadUserInputError(
            f"ERROR: The file {self.source_csv_path} alread exists")

  def get_key_value(self, row:array)-> str:
    key = row[self.sort_colunms_index[0]]
    if len(self.sort_colunms_index) > 1:
      for index in self.sort_colunms_index[1:]:
        key = f"{key}_{row[self.sort_colunms_index[index]]}"
    return key

  def read_line(self, csvfile, index:int):
      line:str = csvfile.readline().rstrip(self.csv_line_sep)
      if line != '':
          line_lenght = len(line)
          self.keys[self.get_key_value(row = line.split(self.csv_column_delimiter, maxsplit=self.max_sort_index+1))] = (index, line_lenght)
      
      index = 0 if line == '' else (index + line_lenght + len(self.csv_line_sep))
      return index

  def start(self)-> None: 

      start_date_time = datetime.now()
      print(f"Start date/time {start_date_time}") 

      with open(self.source_csv_path, mode="r", newline=self.csv_line_sep) as csvfile:
          index=0
          index = self.read_line(csvfile, index) 
          self.row_count += 1
          while index > 0:
              index = self.read_line(csvfile, index) 
              if index > 0:
                self.row_count += 1
      
      process_bar=ProcessBar(total_iteration=self.row_count,
                              start_interator=1,
                              prefix='Progress:',
                              suffix='Complete',
                              length=50)

      process_bar.start()
      sorted_read_values = collections.OrderedDict(sorted(self.keys.items())).values()
      self.keys.clear()
      with open(self.source_csv_path, mode="r", newline=self.csv_line_sep) as f_source_csv:
        with open(self.sorted_destination_csv_path, mode='w', newline='') as f_destinatin_csv:
          # create the csv writer
          writer = csv.writer(f_destinatin_csv,  delimiter=self.csv_column_delimiter, quotechar='"', quoting=csv.QUOTE_MINIMAL)
          written_row_count = 0
          for (row_index, row_length) in sorted_read_values:
              f_source_csv.seek(row_index)
              row = f_source_csv.read(row_length).split(self.csv_column_delimiter)
              writer.writerow(row)
              written_row_count = written_row_count + 1
              process_bar.print_progress_bar(iteration=written_row_count)

      end_date_time = datetime.now() 
      print(f"Finish date/time {end_date_time}") 
      print(f"Total time = {end_date_time - start_date_time}")
      print()

