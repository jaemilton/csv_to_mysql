#!/usr/bin/python3
# -*- coding: UTF-8 -*-
from typing import AnyStr, List
import MySQLdb
from MySQLdb.connections import Connection
from MySQLdb.cursors import Cursor
from array import array
from datetime import datetime
from datetime import timedelta

class MySqlHelper(object):
  """
      Helper do interact with mysql database
      @params:
          host       - Required  : mysql server hostname (Int)
          user       - Required  : usert (Str)
          passwd      - Required  : password (Str)
          db      - Required  : schema name (Str)
          port    - Optional  : mysql server port, default 3306 (Int)
      """


  def __init__(self, 
                host:str,
                user:str,
                passwd:str,
                db:str,
                port:int=3306,
                local_infile=0) -> None:
    self.host = host
    self.user = user
    self.passwd = passwd
    self.db = db
    self.port = port
    self.connection:Connection=None
    self.cursor:Cursor=None
    self.sql_durations:array=[]
    self.local_infile = local_infile


  def _start_connection(self) -> None:
    self.connection = MySQLdb.connect(host = self.host,
                                user = self.user,
                                port = self.port ,
                                passwd = self.passwd,
                                db = self.db,
                                local_infile=self.local_infile)

  def _close_connection(self) -> None:
    if self.connection is not None:
      self.connection.close()
      self.connection = None

  def _insert_many(self, query: str, rows_to_insert:array) -> None:
    self.cursor.executemany(query, rows_to_insert)

  def _exec_sql(self, query:str) -> None:
    self.cursor.execute(query)


  def prep_and_exec_sql(self, 
                      sql_query:str, 
                      sql_data:array=None, 
                      sql_durations_text:str=None,
                      keep_connection=False,
                      commit=True) -> None:
    date_time_start_insert = datetime.now()

    if self.connection is None:
      self._start_connection()

    if self.cursor is None:
      self.cursor = self.connection.cursor()

    close_connection = True if not keep_connection else False

    try:
      if sql_data is None:
        self._exec_sql(sql_query)
      else:
        self._insert_many(sql_query, sql_data)

      if commit:
        self.connection.commit()

      self.cursor.close()
      self.cursor = None
    except Exception as e:
        # Rollback in case there is any error
        self.connection.rollback()
        template = "An exception of type {0} occurred. Arguments:\n{1!r}"
        message = template.format(type(e).__name__, e.args)
        print (message)

    if close_connection:
      self._close_connection()

    date_time_end_insert = datetime.now()
    self.sql_durations.append((">>> " if sql_durations_text is None else sql_durations_text, (date_time_end_insert - date_time_start_insert)))


  def get_sql_durations_messages(self) -> List[AnyStr]:
    durations = []
    count = 0
    total_duration=timedelta(seconds=0)
    for duration_tuple in self.sql_durations:
      count = count + 1
      total_duration = total_duration + duration_tuple[1]
      durations.append(f"[{count} => {duration_tuple[0]} - {duration_tuple[1]}]")
    durations.append(f"Total time on sql database ==> {total_duration}]") 
    return durations

