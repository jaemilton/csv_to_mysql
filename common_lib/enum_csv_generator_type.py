#!/usr/bin/python3
# -*- coding: UTF-8 -*-

from enum import Enum
class CsvGeneratorType(Enum):
  OPTIMIST = 1
  PESSIMIST=2
