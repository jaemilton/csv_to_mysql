#!/usr/bin/python3
# -*- coding: UTF-8 -*-

from enum import Enum
class CsvLinesLoadType(Enum):
  INSERT_ON_FINAL_TABLE_WITH_KEY = 1
  INSERT_ON_TEMP_TABLE_WITH_NO_KEY=2,
  INSERT_ON_INTEMEDIATE_TABLE_WITH_NO_KEY=3
