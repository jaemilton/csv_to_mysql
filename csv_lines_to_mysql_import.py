#!/usr/bin/python3
# -*- coding: UTF-8 -*-
import os, sys, csv
from common_lib.common_base import get_input_parameter_value, valid_mandatory_parameters, print_sql_durations
from common_lib.common_error import BadUserInputError
from common_lib.process_bar import ProcessBar
from common_lib.enum_csv_lines_load_type import CsvLinesLoadType
from common_lib.mysql_helper import MySqlHelper
from common_lib.log_helper import LogHelper
from datetime import datetime
from os.path import dirname, abspath
from dotenv import load_dotenv

path = dirname(abspath(__file__)) + '/.env'
load_dotenv(path)


TABLE_NAME_WITH_PK = "TB_001"
TABLE_NAME_WITHOUT_PK = "TB_001_WITHOUT_KEY"
TEMP_TABLE_NAME_WITHOUT_PK = "TB_TMP_001_WITH_NO_KEY"
CSV_LINE_SEP = '\r\n'

log_helper = None



def load_csv_to_table(csv_path: str, number_of_rows_by_insert: int, load_lines_type:CsvLinesLoadType)-> None: 

    if not os.path.exists(csv_path):
        raise BadUserInputError(
            f"ERROR: The file {csv_path} doesn't exists")

    match load_lines_type:
        case CsvLinesLoadType.INSERT_ON_FINAL_TABLE_WITH_KEY:
            table_name = TABLE_NAME_WITH_PK
        case CsvLinesLoadType.INSERT_ON_INTEMEDIATE_TABLE_WITH_NO_KEY:
            table_name = TABLE_NAME_WITHOUT_PK
        case CsvLinesLoadType.INSERT_ON_TEMP_TABLE_WITH_NO_KEY:
            table_name = TEMP_TABLE_NAME_WITHOUT_PK


    query = f'''INSERT INTO {table_name}
               (
                    COD_ATTR_001,
                    COD_ATTR_002,
                    NUM_ATTR_003,
                    NUM_ATTR_004,
                    NUM_ATTR_005,
                    VLR_ATTR_006,
                    VLR_ATTR_007,
                    VLR_ATTR_008,
                    VLR_ATTR_009,
                    DES_ATTR_010
               )
               VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)'''

    start_date_time = datetime.now()
    log_helper.print_log(f"Start date/time {start_date_time}") 

    row_count  = 0

    with open(csv_path) as fp:
        for _ in fp:
            row_count += 1
    with open(csv_path, newline='') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=';', quotechar='"')
        number_of_lines_read = 0
        total_number_of_lines_read = 0
        rows_to_insert = []
        process_bar=ProcessBar(total_iteration=row_count,
                                start_interator=1,
                                prefix='Progress:',
                                suffix='Complete',
                                length=50)

        process_bar.start()
        keep_connection = True if load_lines_type == CsvLinesLoadType.INSERT_ON_TEMP_TABLE_WITH_NO_KEY else False
        my_sql_helper = MySqlHelper(host = os.getenv("MYSQL_HOSTNAME"),
                                    port = int(os.getenv("MYSQL_PORT")),
                                    user = os.getenv("MYSQL_USER"),
                                    passwd = os.getenv("MYSQL_PASSWD"),
                                    db = os.getenv("MYSQL_SCHEMA"))

        if load_lines_type ==  CsvLinesLoadType.INSERT_ON_TEMP_TABLE_WITH_NO_KEY:
            create_temp_table_sql = f"""CREATE TEMPORARY TABLE {TEMP_TABLE_NAME_WITHOUT_PK} (
                                        `COD_ATTR_001` int(11) NOT NULL,
                                        `COD_ATTR_002` varchar(36) NOT NULL,
                                        `NUM_ATTR_003` int(11) DEFAULT NULL,
                                        `NUM_ATTR_004` int(11) DEFAULT NULL,
                                        `NUM_ATTR_005` int(11) DEFAULT NULL,
                                        `VLR_ATTR_006` decimal(17,2) DEFAULT NULL,
                                        `VLR_ATTR_007` decimal(17,2) DEFAULT NULL,
                                        `VLR_ATTR_008` decimal(15,5) DEFAULT NULL,
                                        `VLR_ATTR_009` decimal(15,5) DEFAULT NULL,
                                        `DES_ATTR_010` varchar(50) DEFAULT NULL,
                                        ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci"""
            
            my_sql_helper.prep_and_exec_sql(sql_query=create_temp_table_sql,
                                            sql_durations_text=f"Creating temp table {TEMP_TABLE_NAME_WITHOUT_PK}",
                                            keep_connection=keep_connection)


        insert_parts_count = 0
        for row in spamreader:
            total_number_of_lines_read = total_number_of_lines_read + 1
            number_of_lines_read = number_of_lines_read + 1
            rows_to_insert.append(row)

            if number_of_lines_read == number_of_rows_by_insert:
                insert_parts_count = insert_parts_count+1
                my_sql_helper.prep_and_exec_sql(sql_query=query,
                                sql_data=rows_to_insert,
                                                sql_durations_text=f"Insert part {insert_parts_count}",
                                                keep_connection=keep_connection)
                rows_to_insert.clear()
                number_of_lines_read = 0
                process_bar.print_progress_bar(iteration=total_number_of_lines_read)
        
    log_helper.print_log("")
    if load_lines_type ==  CsvLinesLoadType.INSERT_ON_INTEMEDIATE_TABLE_WITH_NO_KEY:
        log_helper.print_log(f"Load of table {TABLE_NAME_WITHOUT_PK} finished.")
        log_helper.print_log(f"Starting copy from table {TABLE_NAME_WITHOUT_PK} to {TABLE_NAME_WITH_PK}.")
        sql_copy_data_query =  f'''INSERT INTO {TABLE_NAME_WITH_PK} 
                                    SELECT * FROM {TABLE_NAME_WITHOUT_PK} ORDER BY COD_ATTR_001, COD_ATTR_002'''
        
        my_sql_helper.prep_and_exec_sql(sql_query=sql_copy_data_query,
                            sql_durations_text=f"Copy data from {TABLE_NAME_WITHOUT_PK} to {TABLE_NAME_WITH_PK}")

        log_helper.print_log(f"Cleaning table {TABLE_NAME_WITHOUT_PK}.")
        sql_delete_table_with_no_key = f"TRUNCATE TABLE {TABLE_NAME_WITHOUT_PK}"
        my_sql_helper.prep_and_exec_sql(sql_query=sql_delete_table_with_no_key, 
                            sql_durations_text=f"TRUNCATE TABLE {TABLE_NAME_WITHOUT_PK}")

    elif load_lines_type ==  CsvLinesLoadType.INSERT_ON_TEMP_TABLE_WITH_NO_KEY:
        sql_copy_data_query =  f'''INSERT INTO {TABLE_NAME_WITH_PK} 
                                    SELECT * FROM {TEMP_TABLE_NAME_WITHOUT_PK} ORDER BY COD_ATTR_001, COD_ATTR_002'''
        
        my_sql_helper.prep_and_exec_sql(sql_query=sql_copy_data_query,
                            sql_durations_text=f"Copy data from {TEMP_TABLE_NAME_WITHOUT_PK} to {TABLE_NAME_WITH_PK}",
                            keep_connection=keep_connection)

    
    log_helper.print_log(f"Time spent by insert parts of {number_of_rows_by_insert} row(s)")
    print_sql_durations(my_sql_helper, log_helper)

    end_date_time = datetime.now() 
    log_helper.print_log(f"Finish date/time {end_date_time}") 
    log_helper.print_log(f"Total time = {end_date_time - start_date_time}")

def read_line(keys:dict, csvfile, index):
    line = csvfile.readline().rstrip(CSV_LINE_SEP)
    if line != '':
        line_lenght = len(line)
        keys[line.split(';',2)[1]] = (index, line_lenght)
    
    index = 0 if line == '' else (index + line_lenght + len(CSV_LINE_SEP))
    return index




def start(argv):
    global log_helper
    if (('-h' in argv) or ('-?' in argv)):
        log_helper.print_log("""
        python3 csv_lines_to_mysql_import.py -p CSV_FULL_PATH [-t LOAD_LINES_TYPE] [-n LINES_BY_INSERT] [-h|-?]
        Program to load a csv to a mysql table
        Parameters:
            -p CSV_FULL_PATH  --> mandatory, if not specified will generate csv on current path
            -t LOAD_LINES_TYPE --> optional, define the type of load to be done. 
                Options INSERT_ON_FINAL_TABLE_WITH_KEY|INSERT_ON_TEMP_TABLE_WITH_NO_KEY|INSERT_ON_INTEMEDIATE_TABLE_WITH_NO_KEY
                if not defined will assume INSERT_ON_FINAL_TABLE_WITH_KEY
            -n LINES_BY_INSERT --> optional, number of lines by insert, if not specified, will be 1 line per insert
            -h or -? help
        """)
    
    elif not valid_mandatory_parameters(argv, ['-p']):
        raise BadUserInputError(
            "Input error. To run, call as python3 csv_lines_to_mysql_import.py -p CSV_FULL_PATH [-t LOAD_LINES_TYPE] [-n LINES_BY_INSERT] [-h|-?]")

    else:
        csv_path:str = get_input_parameter_value(argv,'-p')
        load_lines_type_param = get_input_parameter_value(argv,'-t')
        load_lines_type = CsvLinesLoadType.INSERT_ON_FINAL_TABLE_WITH_KEY if load_lines_type_param is None else CsvLinesLoadType[load_lines_type_param]
        number_of_rows_by_insert_param = get_input_parameter_value(argv,'-n')
        number_of_rows_by_insert = 1 if number_of_rows_by_insert_param is None else int(number_of_rows_by_insert_param)
    
    
    
    log_helper = LogHelper(f"{datetime.now().strftime('%Y%m%d%H%M%S')}_{os.path.basename(csv_path).replace('.csv','')}_by_{number_of_rows_by_insert}_lines_to_mysql_import.log")
    load_csv_to_table(csv_path, number_of_rows_by_insert, load_lines_type)



start(sys.argv)
