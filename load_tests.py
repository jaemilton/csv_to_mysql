#!/usr/bin/python3
# -*- coding: UTF-8 -*-

from array import array
from datetime import datetime
from typing import AnyStr, Dict, List
from common_lib.mysql_helper import MySqlHelper
from common_lib.common_error import BadUserInputError
from common_lib.enum_csv_lines_load_type import CsvLinesLoadType
from common_lib.enum_csv_load_type import CsvLoadType
from common_lib.os_process_helper import OsProcessHelper
from common_lib.log_helper import LogHelper
import os, json
from os.path import dirname, abspath
from dotenv import load_dotenv

path = dirname(abspath(__file__)) + '/.env'
load_dotenv(path)


log_helper:LogHelper = LogHelper(f"{datetime.now().strftime('%Y%m%d%H%M%S')}_load_test.log")

def start_process(command:str, 
                    command_params:array, 
                    log_file_name:str,
                    pre_sql_script_list:Dict=None, 
                    pos_sql_script_list:Dict=None):

  
  exec_script_list(pre_sql_script_list, "Executing pre-script")
  log_helper.print_log(f"Command: {command}")
  log_helper.print_log(f"Parameters: {command_params}")
  os_process_helper = OsProcessHelper(command, command_params)
  os_process_helper.start()
  log_helper.print_log(f"################## BEGIN Process: {os_process_helper.get_pid()} ###########################")
  os_process_helper.wait()
  log_helper.print_log(f"################## END   Process: {os_process_helper.get_pid()} ###########################")

  exec_script_list(pos_sql_script_list, "Executing post-script")
  log_helper.print_log(f"Max_cpu_percentage = {os_process_helper.max_cpu_percentage} %")
  log_helper.print_log(f"Max_used_memory = {os_process_helper.max_memory} MB")
  os_process_helper.write_process_monitoring_data_csv(log_file_name)

def exec_script_list(sql_script_list:Dict, message:str):
  if sql_script_list is not None and len(sql_script_list)>0:
    
    log_helper.print_log(message)
    sql_duration_messages=[]
    if sql_script_list is not None:
      for pre_sql_script in sql_script_list:
        sql_duration_messages.extend(exec_script_sql(sql_script=pre_sql_script))

    for message in sql_duration_messages:
      log_helper.print_log(message)

def exec_script_sql(sql_script:Dict) -> List[AnyStr]:
  sql_duration_messages=[]
  if sql_script is not None:
    (pre_sql_script, pre_sql_script_durations_text) = sql_script
    my_sql_helper = MySqlHelper(host = os.getenv("MYSQL_HOSTNAME"),
                                    port = int(os.getenv("MYSQL_PORT")),
                                    user = os.getenv("MYSQL_USER"),
                                    passwd = os.getenv("MYSQL_PASSWD"),
                                    db = os.getenv("MYSQL_SCHEMA"))
    my_sql_helper.prep_and_exec_sql(sql_query=pre_sql_script, sql_durations_text=pre_sql_script_durations_text) 
    sql_duration_messages.extend(my_sql_helper.get_sql_durations_messages())
  return sql_duration_messages

def start_load_by_lines_parts(csv_file_path:str, 
  load_lines_type:CsvLinesLoadType, 
  pre_sql_script_list:Dict=None, 
  pos_sql_script_list:Dict=None, 
  lines_number_part:int=None) -> None:

  if not os.path.exists(csv_file_path):
    raise BadUserInputError(
      f"ERROR: The file {csv_file_path} doesn't exists")
    
  command_params=["csv_lines_to_mysql_import.py", "-p", csv_file_path]

  if lines_number_part is not None and lines_number_part > 0:
    command_params.extend(["-n", str(lines_number_part)])

  command_params.extend(["-t",load_lines_type.name])

  log_file_name = f"logs/{datetime.now().strftime('%Y%m%d%H%M%S')}_load_by_lines_parts.log"
  start_process(command="python", 
                  command_params=command_params, 
                  log_file_name=log_file_name,
                  pre_sql_script_list=pre_sql_script_list,
                  pos_sql_script_list=pos_sql_script_list)

def start_load_data_load(csv_file_path:str, 
  pre_sql_script_list:Dict=None, 
  pos_sql_script_list:Dict=None) -> None:

  if not os.path.exists(csv_file_path):
    raise BadUserInputError(
      f"ERROR: The file {csv_file_path} doesn't exists")
  
  command_params=["csv_file_to_mysql_import.py", "-p", csv_file_path]

  log_file_name = f"logs/{datetime.now().strftime('%Y%m%d%H%M%S')}_load_by_data_load.log"
  start_process(command="python", 
                  command_params=command_params, 
                  log_file_name=log_file_name,
                  pre_sql_script_list=pre_sql_script_list,
                  pos_sql_script_list=pos_sql_script_list)

def start_load_test():
  log_helper.print_log("Starting")
  
  # Opening JSON file
  with open('load_test_cenarios.json') as f:
    # returns JSON object as 
    # a dictionary
    cenarios_json = json.load(f)

  count = 0
  for cenario in cenarios_json["cenarios"]:
    
    start_date_time = datetime.now()
    log_helper.print_log(f"Cenatio start date/time {start_date_time}") 
    
    count=count+1
    load_type = CsvLoadType[cenario["load_type"]]
    log_helper.print_log("########################>START<###############################")
    load_description = None if "description" not in cenario else cenario["description"]
    if load_description is not None:
      log_helper.print_log(f"Load {count} - Description: {load_description}")

    part_count=0
    load_parts=cenario["parts"]
    total_part_count=len(load_parts)
    if total_part_count>1:
      log_helper.print_log(f"Load {count} - Loading {total_part_count} files sequentially.")

    for load_part in load_parts:
      part_count = part_count + 1
      csv_file_path=load_part['csv_file_path']
      
      pre_sql_script_list = get_script_list(load_part, 'pre_scripts')
      pos_sql_script_list = get_script_list(load_part, 'pos_scripts')

      log_helper.print_log(f"Loading file {csv_file_path} - part {part_count} of {total_part_count}")
      sort_type = '>>> an UNSORTED <<<' if load_type == CsvLoadType.INSERT_UNSORTED_FILE_BY_INSERT_LINES else '>>> a SORTED <<<'
      
      if load_type == CsvLoadType.INSERT_UNSORTED_FILE_BY_INSERT_LINES or load_type == CsvLoadType.INSERT_SORTED_FILE_BY_INSERT_LINES:

        load_lines_type= CsvLinesLoadType[load_part['load_lines_type']]
        lines_number_part=None if 'lines_number_part' not in load_part else load_part['lines_number_part']

        log_helper.print_log(f"Cenario >>> {'line by line' if lines_number_part is None else f'by parts of {lines_number_part} lines' } from {sort_type} file.")

        start_load_by_lines_parts(csv_file_path=csv_file_path,
                                  load_lines_type=load_lines_type,
                                  lines_number_part=lines_number_part,
                                  pre_sql_script_list=pre_sql_script_list,
                                  pos_sql_script_list=pos_sql_script_list)
      else:
        
        log_helper.print_log(f"Cenario >>> using LOAD DATA from {sort_type} file.")

        start_load_data_load(csv_file_path=csv_file_path,
                                  pre_sql_script_list=pre_sql_script_list,
                                  pos_sql_script_list=pos_sql_script_list)
  
    end_date_time = datetime.now() 
    log_helper.print_log(f"Cenatio Finish date/time {end_date_time}") 
    log_helper.print_log(f"Cenatio Total time = {end_date_time - start_date_time}")


def get_script_list(load_part:dict, scripts_tag:str):
    sql_script_list=[]
    if scripts_tag in load_part:
      for pre_script in load_part[scripts_tag]:
        sql_script_list.append((pre_script['script'], pre_script['descricao']))
    return sql_script_list

start_load_test()