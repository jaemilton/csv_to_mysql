#!/usr/bin/python3
# -*- coding: UTF-8 -*-

from array import array
from tkinter.messagebox import NO
from typing import Tuple
from common_lib.common_base import get_input_parameter_value, valid_mandatory_parameters
from common_lib.common_error import BadUserInputError
from common_lib.process_bar import ProcessBar
from common_lib.enum_csv_generator_type import CsvGeneratorType
import os, sys, csv, string
import random, decimal
import uuid
from datetime import datetime
from os.path import dirname, abspath
from dotenv import load_dotenv

path = dirname(abspath(__file__)) + '/.env'
load_dotenv(path)

def random_decimal(length: int, digits: int) -> decimal:
    int_part = random.randint(0, int('9'.ljust(length - digits, '9')))
    digits_part = random.randint(0, int('9'.ljust(digits, '9')))
    return decimal.Decimal(f"{int_part}.{digits_part}")

def genetare_csv(csv_path: str, number_of_rows: int, identifyer_param: int, csv_generator_type: CsvGeneratorType):

    if os.path.exists(csv_path):
        raise BadUserInputError(
            f"ERROR: The file {csv_path} alread exists")

    start_date_time = datetime.now()
    print(f"Start date/time {start_date_time}") 
    # open the file in the write mode
    with open(csv_path, mode='w', newline='') as f:
        # create the csv writer
        writer = csv.writer(f,  delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL)

        if identifyer_param is None:
            fk = random.randint(0, 9999)
        else:
            fk = identifyer_param
        
        process_bar=ProcessBar(total_iteration=number_of_rows,
                                start_interator=0,
                                prefix='Progress:',
                                suffix='Complete',
                                length=50)

        process_bar.start()
        random_fields = '11111111'
        if csv_generator_type == CsvGeneratorType.OPTIMIST:
            random_fields = '{0:08b}'.format(random.randint(1, 255))
        
        filds_to_generate = [
            random_fields[0] == '1',
            random_fields[1] == '1',
            random_fields[2] == '1',
            random_fields[3] == '1',
            random_fields[4] == '1',
            random_fields[5] == '1',
            random_fields[6] == '1',
            random_fields[7] == '1'
        ]

        for x in range(number_of_rows):
            # write a row to the csv file
            row = generate_record(fk, filds_to_generate)
            writer.writerow(row)
            process_bar.print_progress_bar(iteration=x)

    end_date_time = datetime.now() 
    print(f"Finish date/time {end_date_time}") 
    print(f"Total time = {end_date_time - start_date_time}")


def generate_record(fk: int, filds_to_generate:array) ->  Tuple:
    return [fk, 
                uuid.uuid4(),
                None if not filds_to_generate[0] else random_int(0, 99999),
                None if not filds_to_generate[1] else random_int(0, 99999), 
                None if not filds_to_generate[2] else random_int(0, 99999), 
                None if not filds_to_generate[3] else random_decimal(17,2),
                None if not filds_to_generate[4] else random_decimal(17,2),
                None if not filds_to_generate[5] else random_decimal(15,5),
                None if not filds_to_generate[6] else random_decimal(15,5),
                None if not filds_to_generate[7] else random_string(50)
            ] 

def random_int(min:int, max:int) -> int:
    return random.randint(min, max)

def random_string(max_length: int) -> str:
    letters = string.ascii_letters
    return ''.join(random.choice(letters) for i in range(random.randint(0, max_length)))


def start(argv):
    if (('-h' in argv) or ('-?' in argv)):
        print("""
        python3 csv_generator.py -n NUMBER_OF_LINES -f CSV_FILE_NAME [-p CSV_PATH] [-i IDENTIFYER] [-h|-?]
        Program to genetare CSV with N lines qith a determined layout
        Parameters:
            -n NUMBER_OF_LINES --> mandatory, number of lines that csv file must have
            -f CSV_FILE_NAME --> mandatory, csv file name
            -p CSV_PATH  --> optional, if not specified will generate csv on current path
            -i IDENTIFYER --> optional, if not specified will be generated a random id,
            -t RANTON_TYPE --> opional, can be OPTIMIST|PESSIMIST, if not set, will assume PESSIMIST
                When OPTIMIST, random fields will have value
                When PESSIMIST, All fields will have value
            -h or -? help
        """)
    
    elif not valid_mandatory_parameters(argv, ['-n', '-f']):
        raise BadUserInputError(
            "Input error. To run, call as python3 csv_generator.py -n NUMBER_OF_LINES -f CSV_FILE_NAME [-p CSV_PATH] [-c IDENTIFYER] [-h|-?]")

    else:
        csv_path = get_input_parameter_value(argv,'-p')
        csv_file_name = get_input_parameter_value(argv,'-f')
        number_of_rows = int(get_input_parameter_value(argv,'-n'))
        identifyer_param = get_input_parameter_value(argv,'-i')

        genetator_type_param = get_input_parameter_value(argv,'-t')
        genetator_type = CsvGeneratorType.PESSIMIST if genetator_type_param is None else CsvGeneratorType[genetator_type_param]

        if not csv_file_name.endswith(".csv"):
            csv_file_name = f'{csv_file_name}.csv'

        if (csv_path is None):
            csv_path = os.path.join(os.getcwd(), csv_file_name)

    genetare_csv(csv_path, number_of_rows, identifyer_param, genetator_type)


start(sys.argv)
