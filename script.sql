CREATE DATABASE `load_test` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;

CREATE TABLE load_test.TB_001 (
  `COD_ATTR_001` int(11) NOT NULL,
  `COD_ATTR_002` varchar(36) NOT NULL,
  `NUM_ATTR_003` int(11) DEFAULT NULL,
  `NUM_ATTR_004` int(11) DEFAULT NULL,
  `NUM_ATTR_005` int(11) DEFAULT NULL,
  `VLR_ATTR_006` decimal(17,2) DEFAULT NULL,
  `VLR_ATTR_007` decimal(17,2) DEFAULT NULL,
  `VLR_ATTR_008` decimal(15,5) DEFAULT NULL,
  `VLR_ATTR_009` decimal(15,5) DEFAULT NULL,
  `DES_ATTR_010` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`COD_ATTR_001`,`COD_ATTR_002`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci

ALTER TABLE `load_test`.`TB_001` 
ADD INDEX `IX_COD_ATTR_002` (`COD_ATTR_002` ASC) VISIBLE;

show global variables like 'local_infile';
set global local_infile=true;




CREATE TABLE load_test.TB_001_WITHOUT_KEY (
  `COD_ATTR_001` int(11) NOT NULL,
  `COD_ATTR_002` varchar(36) NOT NULL,
  `NUM_ATTR_003` int(11) DEFAULT NULL,
  `NUM_ATTR_004` int(11) DEFAULT NULL,
  `NUM_ATTR_005` int(11) DEFAULT NULL,
  `VLR_ATTR_006` decimal(17,2) DEFAULT NULL,
  `VLR_ATTR_007` decimal(17,2) DEFAULT NULL,
  `VLR_ATTR_008` decimal(15,5) DEFAULT NULL,
  `VLR_ATTR_009` decimal(15,5) DEFAULT NULL,
  `DES_ATTR_010` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci


select count(1) from load_test.TB_001;
truncate table load_test.TB_001;
select count(1) from load_test.TB_001;
